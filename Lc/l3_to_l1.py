import sys
from L3.L3 import compile_L3
from L2.L2c import compile_L2
from parser.sexp import parse_sexp, print_sexp, print_prog

# Compile file passed via command line arg
if __name__=='__main__':
    if len(sys.argv) < 2:
        print 'Error: missing L3 program filename'
        sys.exit(0)

    in_file = open(sys.argv[1], 'r')
    program = parse_sexp(in_file.read())
    insts = compile_L3(program)
    result = map(compile_L2, insts)
    if all(result):
        print print_sexp(result)
    else:
        print '"could not register allocate"'
