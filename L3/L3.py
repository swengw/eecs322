from parser.sexp import *
from string import ascii_lowercase
import random
import re
import sys

# Functions to classify an expression

e_insts = set(['let', 'if'])
d_insts = set(['new-array', 'new-tuple', 'aref', 'aset', 'alen', 'print', 'make-closure', 'closure-proc', 'closure-vars'])
label_pat = re.compile(r'^:[a-zA-Z_][a-zA-Z_0-9]*$')
var_pat = re.compile(r'^[a-zA-Z_][a-zA-Z_0-9-]*$')
cmps = set(['<', '<=', '='])
biops = set(['+', '-', '*']) | cmps
preds = set(['number?', 'a?'])

def is_e(inst): return inst[0] in e_insts or is_d(inst)
def is_d(inst):
    return is_v(inst) or inst[0] in d_insts or is_biop(inst[0]) or\
                is_pred(inst[0]) or is_fn_call(inst)
def is_fn_call(inst):
    if not isinstance(inst, list):
        return False
    if inst[0] in d_insts:
        return False
    return all(map(is_v, inst)) and len(inst) >= 1 and len(inst) <= 4
def is_v(part):
    if isinstance(part, int):
        return True
    if not isinstance(part, str):
        return False
    return var_pat.match(part) or label_pat.match(part)
def is_cmp(part): return part in cmps
def is_biop(part): return part in biops
def is_pred(part): return part in preds


# Turn a number into its 2x+1 representation
def encode(num):
    if isinstance(num, int):
        return 2 * num + 1
    return num

# Instructions that encode the value stored in a variable
def get_encode_insts(dest):
    return [[dest, '*=', 2], [dest, '+=', 1]]

# Instructions that check the bounds on an aref or aset
def get_bounds_checking_insts(ph, base, idx, dest, val=None):
    result = []
    tmp = ph.get_fresh_var()
    pass_label = ph.get_new_label()
    fail_label = ph.get_new_label()
    if dest != base and dest != val:
        # Use destination as a temporary variable
        result.append([dest, '<-', encode(idx)])
    else:
        tmp2 = ph.get_fresh_var()
        result.append([tmp2, '<-', encode(idx)])
        dest = tmp2
    result.append([tmp, '<-', ['mem', base, 0]])
    result.append([tmp, '<<=', 1])
    result.append([tmp, '+=', 1])
    result.append(['cjump', dest, '<', tmp, pass_label, fail_label])
    result.append(fail_label)
    result.append(['eax', '<-', ['array-error', base, encode(idx)]])
    result.append(pass_label)
    return result


# These instructions are used for compilation of closure instructions
# as well as the compilation of the original instructions
def compile_new_tuple(args, dest):
    result = []
    n_args = len(args)
    result.append(['eax', '<-', ['allocate', encode(n_args), 0]])
    idx = 4
    for num in args:
        result.append([['mem', 'eax', idx], '<-', encode(num)])
        idx += 4
    if dest != 'eax':
        result.append([dest, '<-', 'eax'])
    return result


def compile_aref(ph, base, idx, dest, do_bounds_checking=False):
    result = []
    if isinstance(base, int):
        # Handle case where the array pointer is a literal
        # This will cause an error, but we will output a valid L2
        # program anyway
        tmp = ph.get_fresh_var()
        result.append([tmp, '<-', base])
        base = tmp
    if do_bounds_checking:
        result += get_bounds_checking_insts(ph, base, idx, dest)
    if isinstance(idx, int):
        result.append([dest, '<-', ['mem', base, (idx+1)*4]])
    else:
        # When second mem operand is not a literal, do add'l math
        result.append([idx, '>>=', 1])
        result.append([idx, '*=', 4])
        result.append([idx, '+=', base])
        result.append([dest, '<-', ['mem', idx, 4]])
    return result


# Extract a string from a series of nested lists
def flatten_to_str(iter):
    if isinstance(iter, str):
        return iter
    elif isinstance(iter, (tuple, list)):
        return ''.join(filter(lambda x: x, map(flatten_to_str, iter)))

# Keeps track of variable names and label names for a particular
# program that's being compiled
class ProgramHelper(object):
    def __init__(self, program):
        self.program_text = flatten_to_str(program)
        self.var_base = self.get_safe_random_string() + '%d'
        self.var_n = 0
        self.label_base = ':' + self.get_safe_random_string() + '%d'
        self.label_n = 0

    # Returns a random string that doesn't appear in this program
    def get_safe_random_string(self):
        s = '__' + ''.join(random.choice(ascii_lowercase) for i in range(8))
        while s in self.program_text:
            s = '__' + ''.join(random.choice(ascii_lowercase) for i in range(8))
        return s

    # Returns a string that hasn't been used as a variable yet
    def get_fresh_var(self):
        self.var_n += 1
        return self.var_base % self.var_n

    # Returns a unique label for use in an if statement
    def get_new_label(self):
        self.label_n += 1
        return self.label_base % self.label_n



# Registers for the first, second, and third function arguments
# Used when compiling function declarations and calls
function_arg_regs = ('ecx', 'edx', 'eax')

# dest is the variable in which the result should be stored;
#  if dest == None (this is at the bottom of an e),
#  store the result in eax
def compile_d(ph, inst, dest=None):
    if not dest:
        dest = 'eax'

    result = []

    if is_v(inst):
        result.append([dest, '<-', encode(inst)])
    elif is_biop(inst[0]):
        if is_cmp(inst[0]):
            result.append([dest, '<-', encode(inst[1]), inst[0], encode(inst[2])])
            result += get_encode_insts(dest)
        elif inst[0] == '*':
            tmp = ph.get_fresh_var()
            result.append([tmp, '<-', encode(inst[1])])
            result.append([tmp, '>>=', 1])
            result.append([dest, '<-', encode(inst[2])])
            result.append([dest, '>>=', 1])
            result.append([dest, '*=', tmp])
            result += get_encode_insts(dest)
        elif inst[0] == '+':
            if inst[1] == dest:
                result.append([dest, '+=', encode(inst[2])])
            elif inst[2] == dest:
                result.append([dest, '+=', encode(inst[1])])
            else:
                result.append([dest, '<-', encode(inst[1])])
                result.append([dest, '+=', encode(inst[2])])
            result.append([dest, '-=', 1])
        else:
            # It's a - instruction
            if inst[1] == dest:
                result.append([dest, '-=', encode(inst[2])])
            elif inst[2] == dest:
                tmp = ph.get_fresh_var()
                result.append([tmp, '<-', dest])
                result.append([dest, '<-', encode(inst[1])])
                result.append([dest, '-=', tmp])
            else:
                result.append([dest, '<-', encode(inst[1])])
                result.append([dest, '-=', encode(inst[2])])
            result.append([dest, '+=', 1])
    elif is_pred(inst[0]):
        if inst[0] == 'a?':
            result.append([dest, '<-', encode(inst[1])])
            result.append([dest, '&=', 1])
            result.append([dest, '*=', -2])
            result.append([dest, '+=', 3])
        else:
            # inst is the number? pred
            result.append([dest, '<-', encode(inst[1])])
            result.append([dest, '&=', 1])
            result.append([dest, '<<=', 1]) # encode result
            result.append([dest, '+=', 1])
    elif inst[0] == 'new-array':
        result.append(['eax', '<-', ['allocate', encode(inst[1]), encode(inst[2])]])
        if dest != 'eax':
            result.append([dest, '<-', 'eax'])
    elif inst[0] == 'new-tuple':
        result += compile_new_tuple(inst[1:], dest)
    elif inst[0] == 'aref':
        result += compile_aref(ph, inst[1], inst[2], dest, True)
    elif inst[0] == 'aset':
        base = inst[1]
        if isinstance(base, int):
            # If first mem operand would be a literal,
            # store it in a variable
            tmp = ph.get_fresh_var()
            result.append([tmp, '<-', encode(base)])
            base = tmp
        result += get_bounds_checking_insts(ph, base, inst[2], dest, inst[3])
        if isinstance(inst[2], int):
            result.append([['mem', base, (inst[2]+1)*4], '<-', encode(inst[3])])
        else:
            # When second mem operand would not be a literal,
            # do additional math
            idx = inst[2]
            result.append([idx, '>>=', 1])
            result.append([idx, '*=', 4])
            result.append([idx, '+=', base])
            result.append([['mem', idx, 4], '<-', encode(inst[3])])
        # Result is always 0
        result.append([dest, '<-', 1])
    elif inst[0] == 'alen':
        result.append([dest, '<-', ['mem', inst[1], 0]])
        result += get_encode_insts(dest)
    elif inst[0] == 'print':
        result.append(['eax', '<-', ['print', encode(inst[1])]])
        if dest != 'eax':
            result.append([dest, '<-', 'eax'])
    elif inst[0] == 'make-closure':
        result += compile_new_tuple([inst[1], inst[2]], dest)
    elif inst[0] == 'closure-proc':
        result += compile_aref(ph, inst[1], 0, dest)
    elif inst[0] == 'closure-vars':
        result += compile_aref(ph, inst[1], 1, dest)
    elif is_fn_call(inst):
        for i, arg in enumerate(inst[1:]):
            result.append([function_arg_regs[i], '<-', encode(arg)])
        if dest == 'eax':
            # This is at the bottom of an e
            result.append(['tail-call', inst[0]])
        else:
            result.append(['call', inst[0]])
            result.append([dest, '<-', 'eax'])
        
    return result


def compile_e(ph, inst):
    result = []
    if isinstance(inst, list) and inst[0] == 'let':
        # Compile the d, store in x
        d = inst[1][0][1]
        x = inst[1][0][0]
        result += compile_d(ph, d, x)
        # Continue with the body
        result += compile_e(ph, inst[2])
    elif isinstance(inst, list) and inst[0] == 'if':
        # Generate a test for the v -
        # assume C convention where nonzero values are true
        then_label = ph.get_new_label()
        else_label = ph.get_new_label()
        result.append(['cjump', encode(inst[1]), '=', encode(0), else_label, then_label])

        # Add label and code for then
        result.append(then_label)
        result += compile_e(ph, inst[2])

        # Add label and code for else
        result.append(else_label)
        result += compile_e(ph, inst[3])
    else:
        result += compile_d(ph, inst)
        if not is_fn_call(inst):
            result.append(['return'])
    return result


def compile_function(ph, fn):
    result = []
    # The function name
    result.append(fn[0])
    # Unpack function args
    for i, name in enumerate(fn[1]):
        result.append([name, '<-', function_arg_regs[i]])
    # Compile body
    result += compile_e(ph, fn[2])
    return result


# Compiles an L3 program into an L2 program
def compile_L3(program):
    ph = ProgramHelper(program)
    result = []

    # Add a call for the first section
    first_label = ph.get_new_label()
    result.append([['call', first_label]])
    result.append([first_label] + compile_e(ph, program[0]))

    for section in program[1:]:
        result.append(compile_function(ph, section))
    return result


# Compile file passed via command line arg
if __name__=='__main__':
    if len(sys.argv) < 2:
        print 'Error: missing L3 program filename'
        sys.exit(0)

    in_file = open(sys.argv[1], 'r')
    program = parse_sexp(in_file.read())
    insts = compile_L3(program)
    print print_prog(insts)
