#from parser.sexp import parse_sexp, print_function
from parser.sexp import *
from L2 import *
import sys

def mem_inst(offset):
    return ['mem', 'ebp', '%s' % offset]

# Whether inst reads the value of var
def reads(inst, var):
    return ((inst[0] == 'call' or inst[0] == 'tail-call') and\
            inst[1] == var) or\
        (len(inst) > 2 and\
            ((inst[1] == '<-' and
                (var in inst[2:]) or\
                (isinstance(inst[0], list) and var in inst[0]) or\
                (isinstance(inst[2], list) and var in inst[2])) or\
            (is_sop(inst[1]) or is_aop(inst[1])) and\
                (inst[0] == var or inst[2] == var))) or\
        (inst[0] == 'cjump' and var in inst[1:4])

def writes(inst, var):
    return inst[0] == var and is_bin_op(inst[1])

def sub_part(part, var, sub):
    if isinstance(part, list):
        return inst_sub(part, var, sub)
    if part == var:
        return sub
    return part

def inst_sub(inst, var, sub):
    return [sub_part(part, var, sub) for part in inst]

def spill(fn, var, offset, prefix):
    insts = []
    mem_sub = mem_inst(offset)
    num = [-1]
    def add(inst):
        insts.append(inst)
    def new_var():
        num[0] += 1
        return prefix + str(num[0])

    # Spill instructions
    for inst in fn:
        if inst == [var, '<-', var]:
            continue
        elif reads(inst, var):
            if inst[1] == '<-' and inst[2] == var and\
                    not isinstance(inst[0], list) and len(inst) == 3:
                add(inst[:2] + [mem_sub])
            else:
                new = new_var()
                add([new, '<-', mem_sub])
                add(inst_sub(inst, var, new))
                if writes(inst, var):
                    add([mem_sub, '<-', new])
        elif writes(inst, var):
            if inst[1] == '<-' and inst[0] == var and\
                    not isinstance(inst[2], list) and len(inst) == 3:
                add([mem_sub] + inst[1:])
            else:
                new = new_var()
                add([new] + inst[1:])
                add([mem_sub, '<-', new])
        else:
            add(inst)

    return insts


if __name__=='__main__':
    if len(sys.argv) < 2:
        print 'Error: missing input L2f filename'
        sys.exit(0)

    in_file = open(sys.argv[1], 'r')
    fn, var, offset, prefix = in_file.read().rsplit(None, 3)
    parsed_fn = parse_sexp(fn)
    result = spill(parsed_fn, var, offset, prefix)
    print print_function(result, False)
