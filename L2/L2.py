import re

cx = set(['eax', 'ecx', 'edx', 'ebx'])
sx = set(['ecx'])
x = cx | set(['esi', 'edi', 'ebp', 'esp'])

aop = set(['+=', '-=', '*=', '&='])
sop = set(['<<=', '>>='])
_cmp = set(['<', '<=', '='])

label_pat = re.compile(r'^:[a-zA-Z_][a-zA-Z_0-9]*$')

def is_cx(s): return s in cx
def is_sx(s): return s in sx
def is_x(s): return s in x

def is_s(s): return is_x(s) or is_num(s) or is_label(s)
def is_u(s): return is_x(s) or is_label(s)
def is_t(s): return is_x(s) or is_num(s)

def is_aop(s): return s in aop
def is_sop(s): return s in sop
def is_cmp(s): return s in _cmp

def is_label(s): return bool(label_pat.match(s))
def is_num(s): return isinstance(s, int)
def is_n4(s): return isinstance(s, int) and int(s) % 4 == 0

def is_bin_op(s): return s == '<-' or is_aop(s) or is_sop(s)
