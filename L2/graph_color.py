from parser.sexp import *
from L2 import *
from liveness import liveness, get_read_vars, get_written_vars
from collections import defaultdict
import sys

real_regs = set(['eax', 'ebx', 'ecx', 'edx', 'edi', 'esi'])
colors = range(6)

def graph_color(insts):
    in_set, out_set = liveness(insts)
    gen = map(get_read_vars, insts)
    kill = map(get_written_vars, insts)

#    for i, j in enumerate(in_set):
#        print insts[i], j, out_set[i], gen[i], kill[i]

    # Create the interference graph
    # =============================

    # intf_graph is a dictionary where:
    # - each key is a register or variable
    # - if a reg/var exists in a key's set, then there's
    #   an edge between the two registers/variables
    intf_graph = defaultdict(set)

    def interfere(var1, var2):
        intf_graph[var1].add(var2)
        intf_graph[var2].add(var1)

    # Build interference graph from slides:
    for i, inst in enumerate(insts):
        # all variables should be in the graph
        for var in gen[i]:
            intf_graph[var] # instantiates key in a defaultdict
        for var in kill[i]:
            intf_graph[var]

        # the variables x and y do not interfere if the
        # instruction was (x <- y)
        remove_write_vars = False
        if len(inst) == 3 and inst[1] == '<-' and\
                isinstance(inst[0], str) and isinstance(inst[2], str) and\
                inst[2] not in intf_graph[inst[0]]:
            remove_write_vars = True

        # two variables live at the same time interfere
        # with each other if they are in the same "out" set
        for var in out_set[i]:
            intf_graph[var].update(out_set[i])

        # killed variables interfere with variables in the out set
        for var in kill[i]:
            for var2 in out_set[i]:
                interfere(var, var2)
                
        # If edges were added from a write inst, remove them
        if remove_write_vars:
            # Don't inadverently add keys to the defaultdict
            if inst[0] in intf_graph:
                intf_graph[inst[0]].discard(inst[2])
            if inst[2] in intf_graph:
                intf_graph[inst[2]].discard(inst[0])
        # constrained arithmetic operators
        elif len(inst) == 5 and inst[1] == '<-':
            interfere(inst[0], 'edi')
            interfere(inst[0], 'esi')
        elif len(inst) == 3 and (inst[1] == '<<=' or inst[1] == '>>=') and\
                isinstance(inst[2], str):
            interf = (real_regs - set(['ecx'])) | set([inst[2]])
            for var in interf:
                intf_graph[var].update(interf)

    # variables that appear in the first instruction's in set
    # interfere with each other
    if in_set:
        for var in in_set[0]:
            intf_graph[var].update(in_set[0])

    # all real registers interfere with each other
    for var in real_regs:
        intf_graph[var].update(real_regs)

    # Remove edges to self
    for var in intf_graph.iterkeys():
        intf_graph[var].discard(var)

    # Put graph in list form for return
    graph_list = []
    for var in sorted(intf_graph.keys()):
        graph_list.append([var] + sorted(intf_graph[var]))


    # Color the graph
    # ===============

    # Pull nodes out of the graph, nodes with fewest edges first
    nodes = sorted(intf_graph.keys(), key=lambda node: len(intf_graph[node]),
                   reverse=True)

    # Attempt to color in order
    coloring = defaultdict(lambda: -1)
    for node in nodes:
        intf_colors = set(map(lambda node: coloring[node], intf_graph[node]))
        for color in colors:
            if color in intf_colors:
                continue
            else:
                coloring[node] = color
                break

    # if there are no nodes with 6 or more edges, then coloring is just empty
    if -1 in coloring.values():
        # coloring failed
        coloring_list = False
    else:
        # Translate colors to variables -
        # each real register should map to itself
        color_regs = {}
        for reg in real_regs:
            color = coloring[reg]
            color_regs[color] = reg

        coloring_list = []
        non_regs = set(coloring.iterkeys()) - real_regs
        for var in sorted(non_regs):
            coloring_list.append([var, color_regs[coloring[var]]])

    return (graph_list, coloring_list)


if __name__=='__main__':
    if len(sys.argv) < 2:
        print 'Error: missing input graph-color filename'
        sys.exit(0)

    in_file = open(sys.argv[1], 'r')
    insts = parse_sexp(in_file.read())
    graph, coloring = graph_color(insts)
    print print_sexp(graph)
    if isinstance(coloring, list):
        print print_sexp(coloring)
    else:
        print '#f'
