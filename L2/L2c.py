import re
import sys
from parser.sexp import *
from L2 import *
from spill import spill
from graph_color import graph_color


# Helpers
# =======
def substitute_variables(parts, sub_dict):
    for i, part in enumerate(parts):
        if isinstance(part, list):
            substitute_variables(part, sub_dict)
        elif part in sub_dict:
            parts[i] = sub_dict[part]

def choose_spill_variable(insts, variables):
    # Choose vars with long live ranges and few uses
    # e.g. callee saves TODO

    if len(variables) == 0:
        return False

#    print sorted(variables)
    return sorted(variables)[0]


# Compilation
# ===========

sub_match = re.compile(r'z_\d+')
def compile_L2(insts):
    spill_count = 0
    sub_prefix = 'z_%d'
    graph, coloring = graph_color(insts)
#    sys.stderr.write('compiling:\n' + str(insts))

    while coloring == False: # it could be an empty list
        # Save callee saves by adding a couple of new vars
        # TODO


        #sys.stderr.write('new iteration\n')
        #sys.stderr.write(str(graph) + '\n')

        # Choose variable to spill
        variables = set([p[0] for p in graph]) - x # remove registers
        variables = filter(lambda x: not sub_match.match(x), variables)
        var = choose_spill_variable(insts, variables)

        if var == False:
            return False

        # Spill that variable
        sub = sub_prefix % spill_count
        insts = spill(insts, var, str(-4 * (spill_count + 1)), sub)
        spill_count += 1

        graph, coloring = graph_color(insts)

    # Perform variable substitutions
    sub_dict = dict(coloring)
    substitute_variables(insts, sub_dict)

    # Adjust esp at beginning of function for spills
    if spill_count > 0:
        esp_inst = ['esp', '-=', 4 * spill_count]
        if not isinstance(insts[0], list) and is_label(insts[0]):
            insts = [insts[0], esp_inst] + insts[1:]
        else:
            # this is main
            insts = [esp_inst] + insts + [['esp', '+=', 4 * spill_count]]

    return insts


if __name__=='__main__':
    if len(sys.argv) < 2:
        print 'Error: missing input L2 filename'
        sys.exit(0)

    in_file = open(sys.argv[1], 'r')
    functions = parse_sexp(in_file.read())
    result = map(compile_L2, functions)
    if all(result):
        print print_sexp(result)
    else:
        print '"could not register allocate"'
