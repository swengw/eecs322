from parser.sexp import *
from L2 import *
from copy import deepcopy
import sys

def get_read_vars(inst):
    if not isinstance(inst, list):
        return set()

    result = set()

    if inst[0] == 'return':
        result.update(['eax', 'esi', 'edi'])
    elif inst[0] == 'call':
        if not is_label(inst[1]):
            result.add(inst[1])
        result.update(['eax', 'ecx', 'edx'])
    elif inst[0] == 'tail-call':
        if not is_label(inst[1]):
            result.add(inst[1])
        result.update(['eax', 'ecx', 'edx', 'esi', 'edi'])
    elif is_aop(inst[1]) or is_sop(inst[1]):
        result.add(inst[0])
        result.add(inst[2])
    elif inst[0] == 'cjump':
        result.add(inst[1])
        result.add(inst[3])
    elif inst[1] == '<-':
        if len(inst) > 3:
            # (cx <- t cmp t)
            result.add(inst[2])
            result.add(inst[4])
        else:
            if isinstance(inst[0], list):
                # This is a ((mem x n4) <- s) instruction
                result.add(inst[0][1])
            if isinstance(inst[2], list):
                # Either a runtime call or a (x <- (mem x n4))
                if inst[2][0] == 'mem':
                    result.add(inst[2][1])
                else:
                    # Runtime call, add components
                    result.update(inst[2][1:])
            elif not is_label(str(inst[2])):
                result.add(inst[2])

    # Remove any number literals that got in
    map(result.remove, filter(lambda x: isinstance(x, int) or x == 'ebp' or x == 'esp', result))

    return result


def get_written_vars(inst):
    if len(inst) == 1:
        return set()

    result = set()

    if inst[0] == 'call':
        result.update(['eax', 'ebx', 'ecx', 'edx'])
    elif is_aop(inst[1]) or is_sop(inst[1]):
        result.add(inst[0])
    elif inst[1] == '<-' and not isinstance(inst[0], list):
        result.add(inst[0])
        if isinstance(inst[2], list) and\
                inst[2][0] in ['print', 'allocate', 'array-error']:
            result.update(['eax', 'ecx', 'edx'])

    return result


def succ(idx, insts):
    """Return the indexes of the successors of the instruction at idx"""

    inst = insts[idx]

    if inst[0] == 'return' or inst[0] == 'tail-call':
        return []
    elif inst[0] == 'goto':
        try:
            return [insts.index(inst[1])]
        except:
            return []
    elif inst[0] == 'cjump':
        result = []
        try:
            result.append(insts.index(inst[4]))
        except:
            pass
        try:
            result.append(insts.index(inst[5]))
        except:
            pass
        return result
    elif len(inst) > 2 and isinstance(inst[2], list)\
            and inst[2][0] == 'array-error':
        return []
    elif idx < len(insts) - 1:
        return [idx+1]

    return []


def update_liveness(insts, in_list, out_list, gen, kill, successors):
    list_len = len(insts)

    # Compute new in_list
    for i in range(list_len):
        in_list[i] = gen[i] | (out_list[i] - kill[i])

    # Compute new out_list
    for i in range(list_len):
        for idx in successors[i]:
            out_list[i].update(in_list[idx])


def liveness(insts):
    gen = map(get_read_vars, insts)
    kill = map(get_written_vars, insts)
    successors = map(succ, range(len(insts)), [insts]*len(insts))
    in_list = [set() for i in range(len(insts))]
    out_list = [set() for i in range(len(insts))]
    old_in_list = []
    old_out_list = []

    while old_in_list != in_list or old_out_list != out_list:
        old_in_list = deepcopy(in_list)
        old_out_list = deepcopy(out_list)
        update_liveness(insts, in_list, out_list, gen, kill, successors)

    return (map(lambda s: sorted(list(s)), in_list), 
            map(lambda s: sorted(list(s)), out_list))


if __name__=='__main__':
    if len(sys.argv) < 2:
        print 'Error: missing input liveness filename'
        sys.exit(0)

    in_file = open(sys.argv[1], 'r')
    insts = parse_sexp(in_file.read())

    in_list, out_list = liveness(insts)
    result_list = [['in'] + in_list, ['out'] + out_list]
    print print_sexp(result_list)
