#lang plai

; Helpers
; -------

(define reserved 
  (list->set `(lambda let letrec if new-tuple begin
                biop pred print new-array aref aset alen
                make-closure closure-vars closure-proc
                + - * < <= = number? a?)))
(define preds (list->set `(number? a?)))
(define biops (list->set `(+ - * < <= =)))
(define prims (set-union preds
                         biops
                         (list->set `(print new-array aref aset alen))))

(define (biop? e)
  (set-member? biops e))

(define (pred? e)
  (set-member? preds e))

(define (prim? e)
  (set-member? prims e))

(define (L5-e? e)
  (not (set-member? reserved e)))

(define (function-app? e)
  (and (list? e) (not (set-member? reserved (car e)))))

(define (label? e)
  (and (symbol? e)
       (regexp-match #rx"^:[a-zA-Z_][a-zA-Z_0-9]*$"
                     (symbol->string e))))

(define label-base ":l5label~a")
(define label-count -1)
(define (fresh-label)
  (set! label-count (add1 label-count))
  (string->symbol (format label-base label-count)))

(define var-base "l5var~a")
(define var-count -1)
(define (fresh-var)
  (set! var-count (add1 var-count))
  (string->symbol (format var-base var-count)))

(define (get-free-vars bound e)
  (match e
    [`(lambda (,args ...) ,body)
     (set-union
       (get-free-vars bound args)
       (get-free-vars (set-union args bound) body))]
    [`(let ([,x ,d]) ,e)
     (set-union
       (get-free-vars bound d)
       (get-free-vars (set-union (set x) bound) e))]
    [(? list?) (apply set-union 
                      (map (λ (x) (get-free-vars bound x)) 
                           e))]
    [else 
     (if (or (set-member? bound e) (set-member? reserved e) (number? e) (label? e))
         null
         (list e))]))

(define procedures '())




; Transformation
; --------------

(define (compile-L5 e)
  (if (prim? e)
      ; doesn't make sense to have a lone prim in L4
      `(1)
      (let ([transformed (transform-L5 e null (hash))])
        (cons
          transformed
          procedures))))


; both bound, to-replace for the letrec transformation
; bound is a list of bound variables
; to-replace is an immutable hash table describing expressions
;   that should be replaced (for letrec transformations)
(define (transform-L5 e bound to-replace)
  (match e
    ; catch let to add x to bound variables
    [`(let ([,x ,d]) ,b)
     `(let ([,x ,(transform-L5 (if (prim? d)
                                   (get-prim-lambda d)
                                   d)
                               bound
                               to-replace)])
           ,(transform-L5 b (cons x bound) to-replace))]

    ; lambda, letrec substitutions as described in notes
    [`(lambda (,args ...) ,body)
       (transform-lambda args 
                         (transform-L5 body (append bound args) to-replace))]


    [`(letrec ([,x ,e1]) ,e2)
     (let ([new-replace (hash-set to-replace x `(aref ,x 0))])
       `(let ([,x (new-tuple 0)])
          (begin (aset ,x 0 ,(transform-L5 e1 bound new-replace))
                 ,(transform-L5 e2 bound new-replace))))]

    
    ; function application
    [(? function-app?)
     (let ([new-var (fresh-var)])
       (let ([transformed-e (map (λ (x)
                                   (if (prim? x)
                                       (transform-L5 (get-prim-lambda x) bound to-replace)
                                       (transform-L5 x bound to-replace)))
                                 e)])
         `(let ([,new-var ,(car transformed-e)])
            ,(if (> (length e) 3)
                 ;; pack args in a tuple if 3 or more args
                 `((closure-proc ,new-var) (closure-vars ,new-var) 
                   (new-tuple ,@(cdr transformed-e)))
                 ;; otherwise pass them as they are
                 `((closure-proc ,new-var) (closure-vars ,new-var) 
                   ,@(cdr transformed-e))))))]
    
    ; normal list
    ; handle prims here
    [(? list?) 
     (cons (transform-L5 (car e) bound to-replace)
           (map (λ (x) 
                  (if (prim? x)
                      (transform-L5 (get-prim-lambda x) bound to-replace)
                      (transform-L5 x bound to-replace)))
                (cdr e)))]
    
    ; perform replace for letrec transformation
    [(? (λ (x)
          (and (not (set-member? bound x)) (hash-ref to-replace x #f))))
(begin
     (hash-ref to-replace e))]

    [else e]))


; returns a lambda of the correct arity
(define (get-prim-lambda prim)
  (match prim
    [(? biop?)
     `(lambda (x y) (,prim x y))]
    [(? pred?)
     `(lambda (x) (,prim x))]
    [`alen
     `(lambda (x) (,prim x))]
    [`print
     `(lambda (x) (,prim x))]
    [`new-array
     `(lambda (x y) (,prim x y))]
    [`aref
     `(lambda (x y) (,prim x y))]
    [`aset
     `(lambda (x y z) (,prim x y z))]))


; Create a L4 function for a lambda expression
; and replace the lambda with a reference to that function
(define (transform-lambda args body)
  (let ([fn-label (fresh-label)])
    (let ([free-vars (get-free-vars (list->set args) body)])
      (if (> (length args) 2)
          (set! procedures
                (cons
                 `(,fn-label (vars-tup args-tup)
                             ,(add-let args 0 (add-let free-vars 0 body `vars-tup) `args-tup))
                 procedures))
          (set! procedures
                (cons
                 `(,fn-label (vars-tup ,@args)
                             ,(add-let free-vars 0 body `vars-tup))
                 procedures)))
      `(make-closure ,fn-label
                     (new-tuple ,@free-vars)))))

; Recursively add the let expressions inside the lifted lambda
(define (add-let vars n e tuple-name)
  (if (empty? vars)
      e
      `(let ([,(car vars) (aref ,tuple-name ,n)])
         ,(add-let (cdr vars) (add1 n) e tuple-name))))







; Read in file
; ------------


(define args (current-command-line-arguments))
(unless (>= (vector-length args) 1)
  (print "Missing filename of L5 program")
  (exit 1))

(define filename (vector-ref args 0))
(define program_file (open-input-file filename #:mode 'text))
(define raw_prog (read program_file))
(close-input-port program_file)

(pretty-display (compile-L5 raw_prog))



; Tests
; -----

#;
(print-only-errors)
#;
(test (get-free-vars `(a b)
                     `(+ (* b x) a y))
      `(x y))
#;
(test (add-let '(a b c) 0 '(+ 1 2))
      `(let ((a (aref vars-tup 0)))
         (let ((b (aref vars-tup 1)))
           (let ((c (aref vars-tup 2)))
             (+ 1 2)))))
#;
(test (compile-L5 `(bob greg stan))
      `(let ((l5var0 bob))
         ((closure-proc l5var0)
          (closure-vars l5var0)
          greg
          stan)))
#;
(test (compile-L5 `(letrec ([y (+ y 1)])
                     (* y 2)))
      `(let ([y (new-tuple 0)])
         (begin (aset y 0 (+ (aref y 0) 1))
                (* (aref y 0) 2))))
