#!/usr/bin/env python

import sys
import lispy

if len(sys.argv) < 2:
    sys.exit('Error: missing filename of L1 program')

l1_filename = sys.argv[1]
with open(l1_filename, 'r') as f:
    l1_raw = lispy.remove_comments(f.read())
    l1_prog = lispy.parse(l1_raw)[0]
    print ''.join(map(lambda x: str(x)+'\n', l1_prog))
